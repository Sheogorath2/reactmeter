
class Controller():
    def AddClicks(self,n):
        self.model.addClicks(n)
    def ClearClicks(self):
        self.model.clearClicks()
    def GetClicks(self):
        return self.model.nclicks

    def AddWrongClicks(self,n):
        self.model.addWrongClicks(n)
    def ClearWrongClicks(self):
        self.model.clearWrongClicks()
    def GetWrongClicks(self):
        return self.model.wrongclicks

    def SetLastTime(self,time):
        self.model.setLastTime(time)
    def SetAvgTime(self,time):
        self.model.setAvgTime(time)
    def GetLastTime(self):
        return self.model.lasttime
    def GetAvgTime(self):
        return self.model.avgtime
    def GetLastTimeMS(self):
        return self.model.lasttime*1000
    def GetAvgTimeMS(self):
        return self.model.avgtime*1000


    def SetModel(self,model):
        self.model=model

    def SetView(self,view):
        self.view=view

    def StartClicking(self, mode):
        self.view.StartClicking(mode)

    def StopClicking(self):
        self.view.StopClicking()

