import sys
from PyQt5.QtWidgets import QApplication
import model, view, controller

# Main Function
if __name__ == '__main__':
    # Create main app
    myApp = QApplication(sys.argv)

    mymodel = model.Model()

    mycontroller = controller.Controller()
    myview = view.View(mycontroller)
    mycontroller.SetModel(mymodel)
    mycontroller.SetView(myview)
    myview.InitUI()

    # Execute the Application and Exit
    myApp.exec_()
    sys.exit()
