class Model():
    nclicks=0
    wrongclicks=0
    lasttime=0
    avgtime=0

    def addClicks(self,nclicks):
        self.nclicks+=nclicks
    def clearClicks(self):
        self.nclicks=0

    def addWrongClicks(self,nclicks):
        self.wrongclicks+=nclicks
    def clearWrongClicks(self):
        self.wrongclicks=0

    def setLastTime(self,time):
        self.lasttime=time
    def setAvgTime(self,time):
        self.avgtime=time
