__author__ = 'herman'

import mainwindow,clicker

class View():
    def __init__(self,controller):
        self.controller=controller
        self.mainwindow=mainwindow.MainWindow(self.controller)
        self.clicker=clicker.Clicker(self.controller)

    def InitUI(self):
        self.mainwindow.InitUI()
        self.clicker.InitUI()
        self.clicker.hide()

    def StartClicking(self, mode):
        self.clicker.setMode(mode)
        print(self.clicker.mode)
        self.clicker.show()
        self.mainwindow.hide()

    def StopClicking(self):
        self.mainwindow.show()
        self.mainwindow.updateStats()
        self.clicker.hide()
