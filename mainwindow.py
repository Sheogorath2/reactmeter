from PyQt5.QtWidgets import QMainWindow
from ui_mainwindow import Ui_MainWindow

class MainWindow(Ui_MainWindow):
    def __init__(self,controller):
        self.controller=controller
        self.container = QMainWindow()

    def connect_slots(self):
        self.btnStart.clicked.connect(self.startClicked)
        self.btnClear.clicked.connect(self.clearClicked)


    def startClicked(self):
        self.controller.StartClicking(self.comboBox.currentText())
        print(self.comboBox.currentText() + " was chosen!")

    def clearClicked(self):
        self.controller.ClearClicks()
        self.controller.ClearWrongClicks()
        self.controller.SetAvgTime(0)
        self.controller.SetLastTime(0)
        self.updateStats()

    def updateStats(self):
        self.lblClicks.setText("You made " + str(self.controller.GetClicks()) + " clicks " +
                               "with " + str(self.controller.GetWrongClicks()) +" wrong clicks"+
                               "\nlast click time was " + str(format(self.controller.GetLastTimeMS(),'.2f'))+
                               " Average time is " + str(format(self.controller.GetAvgTimeMS(),'.2f')))
        if not((self.controller.GetClicks()==0)and(self.controller.GetWrongClicks()==0)):self.btnClear.setEnabled(True)
        else:self.btnClear.setEnabled(False)

    def show(self):
        self.container.show()
    def hide(self):
        self.container.hide()

    def InitUI(self):
        self.setupUi(self.container)
        self.btnClear.setEnabled(False)
        self.connect_slots()
        self.container.show()

