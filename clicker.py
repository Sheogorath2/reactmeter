import random
from threading import Timer
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget
import time
from ui_clicker import Ui_Form
from PyQt5.QtMultimedia import QSound

class Container(QWidget):
    def setChild(self,child):
        self.child=child
    def mousePressEvent(self, event):
        self.child.clicked()
    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Escape:
            self.child.closed()
    def closeEvent(self, QCloseEvent):
        self.child.closed()

class Clicker(Ui_Form):
    colors=["red","green","yellow","blue"]
    _curcolor=0
    isClickingAllowed=False
    isClicked=False
    round=0
    #NROUNDS=5
    def __init__(self,controller):
        self.mode="undefined"
        self.controller = controller
        self.container = Container()
        self.container.setChild(self)
        self.starttime=time.time()
        self.endtime=time.time()

    def setMode(self,mode):
        self.mode=mode.lower()
        self.modePreload()

    def modePreload(self):
        if (self.mode=="sound"):self.sound=QSound("beep.wav")

    def StartClicking(self):
        print("hello")
        self.signaltimer = Timer(2.0,self.ClickMode)
        self.signaltimer.start()
    def ClickMode(self):
        self.signal()
        self.starttime=time.time()
        self.isClickingAllowed=True
        print(self.isClickingAllowed)
        self.clickingallowed=Timer(2.0, self.DisallowClicking)
        self.clickingallowed.start()
        print(self.isClickingAllowed)
        self.isClicked=False
    def DisallowClicking(self):
        self.round+=1
        print("Clicking disallowed")
        self.isClickingAllowed=False
        signaltime=random.randint(2,5)
        self.signaltimer = Timer(signaltime,self.ClickMode)
        self.signaltimer.start()
        if not self.isClicked:
            self.endtime=time.time()
            self.isClicked=True
        self.showtime()

    def showtime(self):
        print(self.starttime)
        print(self.endtime)
        curtime=self.endtime-self.starttime
        self.controller.SetLastTime(curtime)
        if(self.controller.GetAvgTime()==0):self.controller.SetAvgTime(curtime)
        else:
            self.controller.SetAvgTime((self.controller.GetAvgTime()+curtime)/2)
            #print("sssss",(self.controller.GetAvgTime()+curtime)/2)
        print(self.controller.GetLastTime())
    def clicked(self):
        #self.signal()
        if(self.isClickingAllowed):
            if not self.isClicked:
                self.endtime=time.time()
                self.controller.AddClicks(1)
                self.isClicked=True
                self.showtime()
        else: self.controller.AddWrongClicks(1)
        self.lblClicks.setText("You made " + str(self.controller.GetClicks()) + " clicks " +
                               "with " + str(self.controller.GetWrongClicks()) +" wrong clicks"+
                               "\nlast click time was " + str(format(self.controller.GetLastTimeMS(),'.2f'))+
                               " Average time is " + str(format(self.controller.GetAvgTimeMS(),'.2f')))
        #print(self.isClickingAllowed)

    def signal(self):
        if (self.mode=="color"): self.container.setStyleSheet("background-color: " + self.newColor())
        if (self.mode=="sound"):
            self.sound.play()

    def newColor(self):
        self._curcolor+=1
        #print(self._curcolor % len(self.colors))
        return self.colors[self._curcolor % len(self.colors)]

    def closed(self):
        self.t.cancel()
        try:
            self.signaltimer.cancel()
        except: pass
        finally:
            try:
                self.clickingallowed.cancel()
            except: pass
        self.showtime()
        self.controller.StopClicking()

    def show(self):
        self.container.show()
        self.lblClicks.setText("You've chosen <i><u>" + str(self.mode) + "</u></i> mode!")
        self.t=Timer(2.0,self.StartClicking)
        self.t.start()
    def hide(self):
        self.container.hide()

    def InitUI(self):
        self.setupUi(self.container)
        self.container.show()
