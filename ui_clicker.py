# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_clicker.ui'
#
# Created: Sat Sep 26 11:24:28 2015
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(400, 316)
        Form.setWindowOpacity(1.0)
        self.gridLayout_2 = QtWidgets.QGridLayout(Form)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.lblClicks = QtWidgets.QLabel(Form)
        self.lblClicks.setObjectName("lblClicks")
        self.gridLayout.addWidget(self.lblClicks, 0, 0, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem, 2, 0, 1, 1)
        self.lblInfo = QtWidgets.QLabel(Form)
        font = QtGui.QFont()
        font.setPointSize(7)
        self.lblInfo.setFont(font)
        self.lblInfo.setObjectName("lblInfo")
        self.gridLayout.addWidget(self.lblInfo, 3, 0, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 1, 1, 1)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.lblClicks.setText(_translate("Form", "Number of your clicks"))
        self.lblInfo.setText(_translate("Form", "Press ESCAPE to exit"))

